# Sample Payments API 

*Created and maintained by Tasos Tsaousis (tasoskanenas@gmail.com)*

- *Online Documentation: https://documenter.getpostman.com/view/669883/payments-api/RVu5jUTr*

- *Local Documentation: http://localhost:8080/swagger-ui.html*

- *Documentation in PDF: ./docs/Payments-API-Docs.pdf*
