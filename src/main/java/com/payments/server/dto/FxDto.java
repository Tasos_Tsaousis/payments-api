package com.payments.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FxDto {

    @NotNull
    private String contract_reference;
    @NotNull
    private Double exchange_rate;
    @NotNull
    private Double original_amount;
    @NotNull
    private String original_currency;
}
