package com.payments.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChargesInformationDto {

    @NotNull
    private String bearer_code;
    @NotNull
    private List<ChargeDataDto> sender_charges;
    @NotNull
    private Double receiver_charges_amount;
    @NotNull
    private String receiver_charges_currency;
}
