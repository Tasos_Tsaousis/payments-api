package com.payments.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LiteAccountDto {

    @NotNull
    private String account_number;
    @NotNull
    private Integer bank_id;
    @NotNull
    private String bank_id_code;
}
