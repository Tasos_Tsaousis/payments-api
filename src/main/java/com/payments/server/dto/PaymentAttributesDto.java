package com.payments.server.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentAttributesDto {

    @NotNull
    private AccountDto beneficiary_party;
    @NotNull
    private ChargesInformationDto charges_information;
    @NotNull
    private AccountDto debtor_party;
    @NotNull
    private FxDto fx;
    @NotNull
    private Double amount;
    @NotNull
    private String currency;
    @NotNull
    private String end_to_end_reference;
    @NotNull
    private Integer numeric_reference;
    @NotNull
    private String payment_id;
    @NotNull
    private String payment_purpose;
    @NotNull
    private String payment_scheme;
    @NotNull
    private String payment_type;
    @NotNull
    private String processing_date;
    @NotNull
    private String reference;
    @NotNull
    private String scheme_payment_type;
    @NotNull
    private String scheme_payment_sub_type;
    @NotNull
    private LiteAccountDto sponsor_party;
}
