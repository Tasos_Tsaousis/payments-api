package com.payments.server.controller;

import com.payments.server.dto.PaymentDto;
import com.payments.server.exception.NotFoundException;
import com.payments.server.service.PaymentService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RequestMapping("/api/payments")
@RestController
@Api(value = "/api/payments", tags = "Payments", description = "CRUD Operations about payments.")
public class PaymentsController {

    @Autowired
    private PaymentService paymentService;

    @GetMapping(value = "", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllPayments() {
        try {
        return new ResponseEntity<>(paymentService.getAllPayments(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>("{ \"message\": \"An error has occurred while retrieving request data.\" }", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addPayment(@Valid @RequestBody PaymentDto dto, BindingResult result) throws Exception {
        if (result.hasErrors()) {
            return new ResponseEntity<>(result.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        try {
            PaymentDto createddDto = paymentService.createPayment(dto);
            if(createddDto == null){
                return new ResponseEntity<>("{ \"message\": \"An error has occurred while inserting request data.\" }", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(createddDto, HttpStatus.CREATED);
        } catch (ConstraintViolationException cve){
            log.error(cve.getMessage());
            return new ResponseEntity<>("{ \"message\": \"Invalid request data.\" }", HttpStatus.BAD_REQUEST);
        } catch (DataIntegrityViolationException dive){
            log.error(dive.getMessage());
            return new ResponseEntity<>("{ \"message\": \"There has been a conflict with request data.\" }", HttpStatus.CONFLICT);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>("{ \"message\": \"An error has occurred while inserting request data.\" }", HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @GetMapping(value = "/{paymentId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPayment(@PathVariable String paymentId) {
        PaymentDto foundPaymentDto = paymentService.retrievePayment(paymentId);
        if (foundPaymentDto == null) {
            return new ResponseEntity<>("{ \"message\": \"Request data not found.\" }", HttpStatus.NOT_FOUND);

        }
        return new ResponseEntity<>(foundPaymentDto, HttpStatus.OK);
    }


    @PutMapping(value = "/{paymentId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updatePayment(@PathVariable String paymentId, @Valid @RequestBody PaymentDto dto,
                                           BindingResult result) throws Exception {
        if (result.hasErrors()) {
            return new ResponseEntity<>(result.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        try {
            PaymentDto updatedDto = paymentService.updatePayment(paymentId, dto);
            if(updatedDto == null){
                return new ResponseEntity<>("{ \"message\": \"An error has occurred while updating request data.\" }", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity<>(updatedDto, HttpStatus.OK);
        } catch (ConstraintViolationException cve){
            log.error(cve.getMessage());
            return new ResponseEntity<>("{ \"message\": \"Invalid request data.\" }", HttpStatus.BAD_REQUEST);
        } catch (DataIntegrityViolationException dive) {
            log.error(dive.getMessage());
            return new ResponseEntity<>("{ \"message\": \"There has been a conflict with request data.\" }", HttpStatus.CONFLICT);
        } catch(NotFoundException nfe){
            log.error(nfe.getMessage());
            return new ResponseEntity<>("{ \"message\": \"Update request data not found.\" }", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return new ResponseEntity<>("{ \"message\": \"An error has occurred while updating request data.\" }", HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @DeleteMapping(value = "/{paymentId}")
    public ResponseEntity<?> deletePayment(@PathVariable String paymentId) throws Exception {
        try {
            Boolean result = paymentService.deletePayment(paymentId);
            if (result) {
                return new ResponseEntity<>("{ \"message\": \"Successfully deleted request data.\" }",HttpStatus.OK);
            } else {
                return new ResponseEntity<>("{ \"message\": \"Delete request data not found.\" }", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>("{ \"message\": \"An error has occurred while deleting request data.\" }", HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }
}
