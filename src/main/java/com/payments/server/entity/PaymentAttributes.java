package com.payments.server.entity;

import com.payments.server.domain.*;
import com.payments.server.dto.PaymentAttributesDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Data
@Table(name="attributes")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentAttributes implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name="beneficiary_party_id")
	@NotNull
	private BeneficiaryParty beneficiaryParty;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name="charges_information_id")
	@NotNull
	private ChargesInformation chargesInformation;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name="debtor_party_id")
	@NotNull
	private DebtorParty debtorParty;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name="sponsor_party_id")
	@NotNull
	private SponsorParty sponsorParty;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name="fx_id")
	@NotNull
	private ForeignExchangeData fx;

	@Column(name="amount")
	@NotNull
	private Double amount;

	@Column(name="currency")
	@NotNull
	private Currency currency;

	@Column(name="end_to_end_reference")
	@NotNull
	private String endToEndReference;

	@Column(name="numeric_reference")
	@NotNull
	private Integer numericReference;

	@Column(name="payment_id")
	@NotNull
	private String paymentId;

	@Column(name="payment_purpose")
	@NotNull
	private String paymentPurpose;

	@Column(name="payment_scheme")
	@NotNull
	private PaymentSchemeType paymentScheme;

	@Column(name="payment_type")
	@NotNull
	private PaymentType paymentType;

	@Column(name="processing_date")
	@NotNull
	private Timestamp processingDate;

	@Column(name="reference")
	@NotNull
	private String reference;

	@Column(name="scheme_payment_type")
	@NotNull
	private SchemePaymentType schemePaymentType;

	@Column(name="scheme_payment_sub_type")
	@NotNull
	private SchemePaymentSubtype schemePaymentSubtype;

	public static PaymentAttributesDto toDto(PaymentAttributes pa){
		return PaymentAttributesDto
				.builder()
				.beneficiary_party(Account.toDto(pa.getBeneficiaryParty()))
				.charges_information(ChargesInformation.toDto(pa.getChargesInformation()))
				.debtor_party(Account.toDto(pa.getDebtorParty()))
				.fx(ForeignExchangeData.toDto(pa.getFx()))
				.amount(pa.getAmount())
				.currency(pa.getCurrency().name())
				.end_to_end_reference(pa.getEndToEndReference())
				.numeric_reference(pa.getNumericReference())
				.payment_id(pa.getPaymentId())
				.payment_purpose(pa.getPaymentPurpose())
				.payment_scheme(pa.getPaymentScheme().name())
				.payment_type(pa.getPaymentType().name())
				.processing_date(new SimpleDateFormat("yyyy-MM-dd").format(pa.getProcessingDate()))
				.reference(pa.getReference())
				.scheme_payment_type(pa.getSchemePaymentType().name())
				.scheme_payment_sub_type(pa.getSchemePaymentSubtype().name())
				.sponsor_party(Account.toLiteDto(pa.getSponsorParty()))
				.build();
	}

	public static PaymentAttributes fromDto(PaymentAttributesDto dto) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date parsedDate = dateFormat.parse(dto.getProcessing_date());
		Timestamp processingDate = new java.sql.Timestamp(parsedDate.getTime());
		return PaymentAttributes
				.builder()
				.beneficiaryParty(BeneficiaryParty.fromDto(dto.getBeneficiary_party()))
				.chargesInformation(ChargesInformation.fromDto(dto.getCharges_information()))
				.debtorParty(DebtorParty.fromDto(dto.getDebtor_party()))
				.fx(ForeignExchangeData.fromDto(dto.getFx()))
				.amount(dto.getAmount())
				.currency(Currency.valueOf(dto.getCurrency()))
				.endToEndReference(dto.getEnd_to_end_reference())
				.numericReference(dto.getNumeric_reference())
				.paymentId(dto.getPayment_id())
				.paymentPurpose(dto.getPayment_purpose())
				.paymentScheme(PaymentSchemeType.valueOf(dto.getPayment_scheme()))
				.paymentType(PaymentType.valueOf(dto.getPayment_type()))
				.processingDate(processingDate)
				.reference(dto.getReference())
				.schemePaymentType(SchemePaymentType.valueOf(dto.getScheme_payment_type()))
				.schemePaymentSubtype(SchemePaymentSubtype.valueOf(dto.getScheme_payment_sub_type()))
				.sponsorParty(SponsorParty.fromLiteDto(dto.getSponsor_party()))
				.build();
	}

}
