package com.payments.server.entity;

import com.payments.server.domain.Currency;
import com.payments.server.domain.FinancialActionChargesType;
import com.payments.server.dto.ChargeDataDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="charge_data")
@Builder
public class ChargeData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="type")
	@NotNull
	private FinancialActionChargesType type;

	@NotNull
	@Column(name="amount")
	private Double amount;

	@NotNull
	@Column(name="currency")
	private Currency currency;


	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "charges_information_id")
	private ChargesInformation chargesInformation;

	public static ChargeDataDto toDto(ChargeData chargeData) {

		return ChargeDataDto
                .builder()
                .amount(chargeData.getAmount())
                .currency(chargeData.getCurrency().name())
                .build();
	}

	public static ChargeData fromDto(ChargesInformation ci,ChargeDataDto dto){

		return ChargeData
                .builder()
                .amount(dto.getAmount())
                .currency(Currency.valueOf(dto.getCurrency()))
				.chargesInformation(ci)
		        .build();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("ChargeData{");
		sb.append("id=").append(id);
		sb.append(", type=").append(type);
		sb.append(", amount=").append(amount);
		sb.append(", currency=").append(currency);
		sb.append('}');
		return sb.toString();
	}
}
