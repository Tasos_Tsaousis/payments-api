package com.payments.server.entity;

import com.payments.server.domain.BankIdCodeType;
import com.payments.server.domain.AccountNumberCodeType;
import com.payments.server.dto.LiteAccountDto;
import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Data
@Entity
@DiscriminatorValue("SponsorParty")
@PrimaryKeyJoinColumn(name="id")
public class SponsorParty extends Account {

	public SponsorParty(Long id, String accountNumber, String accountName, AccountNumberCodeType accountNumberCode, Integer accountType,
                        String address, Integer bankId, BankIdCodeType bankIdCode, String name) {
		super(id, accountNumber, accountName, accountNumberCode, accountType, address, bankId, bankIdCode, name);
	}

	public SponsorParty() {
		super();
	}


	public static SponsorParty fromLiteDto(LiteAccountDto dto){
		SponsorParty sp = new SponsorParty();
		sp.setAccountNumber(dto.getAccount_number());
		sp.setBankId(dto.getBank_id());
		sp.setBankIdCode(BankIdCodeType.valueOf(dto.getBank_id_code()));

		return sp;

	}

}
