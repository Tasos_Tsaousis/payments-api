package com.payments.server.entity;

import com.payments.server.domain.BankIdCodeType;
import com.payments.server.dto.AccountDto;
import com.payments.server.domain.AccountNumberCodeType;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@DiscriminatorValue("BeneficiaryParty")
@PrimaryKeyJoinColumn(name="id")
public class BeneficiaryParty extends Account {

	public BeneficiaryParty(Long id, String accountNumber, String accountName, AccountNumberCodeType accountNumberCode, Integer accountType,
                            String address, Integer bankId, BankIdCodeType bankIdCode, String name) {
		super(id, accountNumber, accountName, accountNumberCode, accountType, address, bankId, bankIdCode, name);
	}

	public BeneficiaryParty() {
		super();
	}

	public static BeneficiaryParty fromDto(AccountDto dto){
		BeneficiaryParty bp = new BeneficiaryParty();
		bp.setAccountNumber(dto.getAccount_number());
		bp.setAccountName(dto.getAccount_name());
		bp.setAccountNumberCode(AccountNumberCodeType.valueOf(dto.getAccount_number_code()));
		bp.setAccountType(dto.getAccount_type());
		bp.setAddress(dto.getAddress());
		bp.setBankId(dto.getBank_id());
		bp.setBankIdCode(BankIdCodeType.valueOf(dto.getBank_id_code()));
		bp.setName(dto.getName());

		return bp;
	}

}
