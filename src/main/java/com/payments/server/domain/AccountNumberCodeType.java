package com.payments.server.domain;

public enum AccountNumberCodeType {
	IBAN,BBAN
}
