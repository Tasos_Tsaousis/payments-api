package com.payments.server.service;

import com.payments.server.dto.PaymentDto;
import com.payments.server.dto.PaymentListDto;

public interface PaymentService {

    PaymentDto createPayment(PaymentDto dto) throws Exception;

    PaymentDto retrievePayment(String id);

    PaymentDto updatePayment(String paymentId, PaymentDto dto) throws Exception;

    Boolean deletePayment(String id);

    PaymentListDto getAllPayments();
}
