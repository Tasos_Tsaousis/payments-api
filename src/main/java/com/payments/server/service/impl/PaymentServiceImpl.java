package com.payments.server.service.impl;

import com.payments.server.dto.PaymentDto;
import com.payments.server.dto.PaymentListDto;
import com.payments.server.entity.Payment;
import com.payments.server.exception.NotFoundException;
import com.payments.server.repository.PaymentRepository;
import com.payments.server.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("paymentService")
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository repository;

    @Override
    public PaymentDto createPayment(PaymentDto dto) throws Exception {
        Payment newPayment = Payment.fromDto(dto);
        Payment savedPayment = repository.save(newPayment);
        return savedPayment == null ? null : Payment.toDto(savedPayment);
    }

    @Override
    public PaymentDto retrievePayment(String id) {
        Payment retrievedPayment = repository.findByPaymentId(id);
        return retrievedPayment == null ? null : Payment.toDto(retrievedPayment);
    }

    @Override
    public PaymentDto updatePayment(String paymentId, PaymentDto dto) throws Exception {
        Payment retrievedPayment = repository.findByPaymentId(paymentId);
        if(retrievedPayment == null){
            throw new NotFoundException();
        }
        Payment toBePersistedPayment = Payment.fromDto(dto);
        toBePersistedPayment.setId(retrievedPayment.getId());
        Payment updatedPayment = repository.save(toBePersistedPayment);
        return updatedPayment == null ? null : Payment.toDto(updatedPayment);
    }

    @Override
    public Boolean deletePayment(String paymentId) {
        Payment retrievedPayment = repository.findByPaymentId(paymentId);
        if (retrievedPayment == null) {
            return false;
        }
        repository.delete(retrievedPayment);
        return true;
    }

    @Override
    public PaymentListDto getAllPayments() {
        List<Payment> foundPayments;
        List<PaymentDto> foundPaymentDtos = new ArrayList<>();
        foundPayments = repository.findAll();
        for (Payment p : foundPayments) {
            foundPaymentDtos.add(Payment.toDto(p));
        }
        PaymentListDto dto = new PaymentListDto();
        dto.setData(foundPaymentDtos);
        return dto;
    }
}
