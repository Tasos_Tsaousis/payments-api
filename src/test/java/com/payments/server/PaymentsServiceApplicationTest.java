package com.payments.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentsServiceApplicationTest {

    public static void main(String[] args) {
        SpringApplication.run(PaymentsServiceApplicationTest.class, args);
    }

}

