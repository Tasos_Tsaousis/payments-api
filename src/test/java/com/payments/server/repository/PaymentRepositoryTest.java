package com.payments.server.repository;

import com.payments.server.AbstractTestConfiguration;
import com.payments.server.entity.Payment;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNull;
import static junit.framework.TestCase.assertNotNull;

public class PaymentRepositoryTest extends AbstractTestConfiguration {

	@Autowired
	private PaymentRepository paymentRepository;

	private static List<Payment> payments = new ArrayList<>();


	@Before
	public void initSuite() {
		Payment payment = null;
		try {
			payment = Payment.fromDto(createTestPayment("testRepo1"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		payments.add(payment);
	}

	@Test
	public void testSave() {
		Payment payment = paymentRepository.save(payments.get(0));
		assertNotNull(payment);
		System.out.println(payment);
	}

	@Test
	public void testFindAll() {
		Payment payment = paymentRepository.save(payments.get(0));
		List<Payment> retrievedPayments = paymentRepository.findAll();
		assert(retrievedPayments.size() > 0);
	}

	@Test
	public void testFindByPaymentId() {
		Payment payment = paymentRepository.save(payments.get(0));
		Payment retrievedPayment = paymentRepository.findByPaymentId(payment.getPaymentUuid());
		assertNotNull(retrievedPayment);
	}

	@Test
	public void testDelete(){
		Payment payment = paymentRepository.save(payments.get(0));
		Payment retrievedPayment = paymentRepository.findByPaymentId(payment.getPaymentUuid());
		assertNotNull(retrievedPayment);
		paymentRepository.delete(payment);
		Payment deletedPayment = paymentRepository.findByPaymentId(payment.getPaymentUuid());
		assertNull(deletedPayment);

	}
}
